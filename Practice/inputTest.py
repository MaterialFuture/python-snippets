# Example of app from manual import
applicant = input("Enter the applicant's name: ")
interviewer = input("Enter the interviewer's name: ")
time = input("Enter the appointment time: ")

print(interviewer, "will interview", applicant, "at", time)


# Example of app from local CSV Data
import csv

row = ['2', ' Marie', ' California']

with open('people.csv', 'r') as readFile:
reader = csv.reader(readFile)
lines = list(reader)
lines[2] = row

with open('people.csv', 'w') as writeFile:
    writer = csv.writer(writeFile)
    writer.writerows(lines)

readFile.close()
writeFile.close()


# Example of automated app from external data
from API import data
print(data.interviewer, "will interview", data.applicant, "at", data.time)
export
