import requests
import json

from BeautifulSoup import BeautifulSoup

r = requests.get('https://news.ycombinator.com/')
soup = BeautifulSoup(r.text)
outer_table = soup.find('table')
inner_table = outer_table.findAll('table')[1]
rows = inner_table.findAll('tr')
stories = []
rows_per_story = 3

for row_num in range(0, len(rows)-rows_per_story, rows_per_story):
    story_pieces = rows[row_num].findAll('td')
    meta_pieces = rows[row_num + 1].findAll('td')
    story = { 'current_position': story_pieces[0].string, 'link': story_pieces[2].find("a")['href'], 'title': story_pieces[2].find('a').string, }
    try:
        story['posted_by'] = meta_pieces[1].findAll('a')[0].string
    except IndexError:
        continue

print json.dumps(stories, indent=1)
